<?php

namespace VerisureLab\Library\LeadTransmitter\Service;

use Enqueue\Client\Config;
use Interop\Queue\Context;
use VerisureLab\Library\LeadTransmitter\ValueObject\Lead;

class Transmitter
{
    /**
     * @var Context
     */
    private $context;

    /**
     * @var string
     */
    private $queueName;

    public function __construct(Context $context, string $queueName)
    {
        $this->context = $context;
        $this->queueName = $queueName;
    }

    public function send(Lead $lead): void
    {
        $fooQueue = $this->context->createQueue($this->queueName);

        $message = $this->context->createMessage(json_encode($lead->toArray()));
        $message->setProperty(Config::COMMAND, 'transmit_lead');
        $message->setProperty(Config::CONTENT_TYPE, 'application/json');
        $message->setProperty(Config::PROCESSOR, 'Application\AntiCorruptionLayer\Processor\LeadTransmittedProcessor');

        $this->context->createProducer()->send($fooQueue, $message);
    }
}