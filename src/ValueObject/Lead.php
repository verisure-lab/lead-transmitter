<?php

namespace VerisureLab\Library\LeadTransmitter\ValueObject;

class Lead
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var array
     */
    protected $raw;

    protected function __construct(string $id, array $raw)
    {
        $this->id = $id;
        $this->raw = $raw;
    }

    public static function fromArray(array $data): Lead
    {
        return new static(
            $data['id'],
            $data['raw']
        );
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Get raw
     *
     * @return array
     */
    public function getRaw(): array
    {
        return $this->raw;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'raw' => $this->raw,
        ];
    }
}