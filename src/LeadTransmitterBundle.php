<?php

namespace VerisureLab\Library\LeadTransmitter;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use VerisureLab\Library\LeadTransmitter\DependencyInjection\LeadTransmitterExtension;

class LeadTransmitterBundle extends Bundle
{
    public function getContainerExtension(): LeadTransmitterExtension
    {
        return new LeadTransmitterExtension();
    }
}